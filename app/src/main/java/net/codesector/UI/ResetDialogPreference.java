package net.codesector.UI;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;

import net.codesector.Mileage.Config;
import net.codesector.Mileage.R;

public class ResetDialogPreference extends DialogPreference {
    protected Context context;

    public ResetDialogPreference(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.context = context;
    }

    @Override
    public void onClick(DialogInterface dialog, int which)
    {
        super.onClick(dialog, which);

        if(which == DialogInterface.BUTTON_POSITIVE)
        {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences( this.context );
            SharedPreferences.Editor editor = preferences.edit();
            String user = preferences.getString( Config.PREF_USER, null );

            editor.clear();

            editor.putString( Config.PREF_USER, user );
            editor.commit();

            PreferenceManager.setDefaultValues( context, R.xml.preferences, false );

            OnPreferenceChangeListener listener = getOnPreferenceChangeListener();
            if ( listener != null )
                listener.onPreferenceChange(this, true);
        }
    }
}