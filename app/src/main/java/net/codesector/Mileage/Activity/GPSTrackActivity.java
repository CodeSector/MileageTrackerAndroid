package net.codesector.Mileage.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import net.codesector.Mileage.JsonLocation;
import net.codesector.Mileage.R;
import net.codesector.Mileage.Service.LocationService;
import net.codesector.Mileage.Utils;

import java.util.ArrayList;

public class GPSTrackActivity extends BaseActivity {

    public static final String BROADCAST_UPDATES = "net.codesector.Mileage.GPS_UPDATES";

    private Button btnTrack;
    private TextView lblAccuracy, lblMileage, lblSpeed;
    private GoogleMap map;

    private LocationService service;

    private Boolean isTracking = false;

    private double mileage, maxSpeed;
    private Location firstLocation = null, lastLocation = null, lastUpdateLocation = null;
    private long start, stop, lastUpdate;
    private int updateInterval, updateDistance;
    private ArrayList<JsonLocation> gpsPath = new ArrayList<>();

    private int lineColor, lineWidth, zoomLevel;

    @Override
    protected void onCreate( Bundle state ) {
        super.onCreate( state );
        setContentView( R.layout.activity_gps );

        getActionBar().setDisplayHomeAsUpEnabled( true );

        lineWidth = preferences.getInt( getResources().getString( R.string.pref_map_line_width ), getResources().getInteger( R.integer.default_map_line_width ) );
        lineColor = preferences.getInt( getResources().getString( R.string.pref_map_line_color ), Color.MAGENTA );
        zoomLevel =  preferences.getInt( getResources().getString( R.string.pref_map_zoom_level ), getResources().getInteger( R.integer.default_map_zoom_level ) );

        btnTrack = (Button) findViewById( R.id.btn_gps_track );
        lblAccuracy = (TextView) findViewById( R.id.lbl_accuracy );
        lblMileage = (TextView) findViewById( R.id.lbl_mileage );
        lblSpeed = (TextView) findViewById( R.id.lbl_speed );
        ((MapFragment) getFragmentManager().findFragmentById( R.id.map )).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                init();
            }
        });
    }

    protected void init() {
        map.getUiSettings().setZoomControlsEnabled( true );
        map.setMyLocationEnabled( true );

        service = new LocationService( this, BROADCAST_UPDATES );
        Location loc = service.getLocation();
        if ( loc != null )
            map.animateCamera( CameraUpdateFactory.newLatLngZoom( new LatLng( loc.getLatitude(), loc.getLongitude() ), zoomLevel ));

        btnTrack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( isTracking ) {
                    btnTrack.setEnabled( false );
                    isTracking = false;
                    service.stop();

                    if ( mileage > 0f ) {
                        stop = System.currentTimeMillis();
                        double span = Utils.toHours( Utils.timeSpan( (double) start, (double) stop ));
                        Intent i = new Intent( GPSTrackActivity.this, CreateTripActivity.class );
                        i.putExtra( "gps_data", gpsPath );
                        i.putExtra( "mileage", mileage );
                        i.putExtra( "max_speed", maxSpeed );
                        i.putExtra( "average_speed", ( span != 0 ) ? mileage / span : 0 );

                        String start, end;
                        start = Utils.locationToString( firstLocation, getApplicationContext() );
                        end = Utils.locationToString( lastLocation, getApplicationContext() );

                        if ( !start.isEmpty() && !end.isEmpty())
                            i.putExtra( "title", String.format( "%s to %s", start, end ));
                        startActivity(i);

                        finish();
                    } else {
                        btnTrack.setEnabled( true );
                        btnTrack.setText( R.string.btn_gps_track_start );
                    }
                } else {
                    updateInterval = preferences.getInt( getResources().getString( R.string.pref_gps_interval ), getResources().getInteger( R.integer.default_gps_interval ));
                    updateDistance =  preferences.getInt( getResources().getString( R.string.pref_gps_distance ), getResources().getInteger( R.integer.default_gps_distance ));
                    // service.setInterval( updateInterval );
                    //service.setDistance( updateDistance );
                    isTracking = service.start();
                    if ( isTracking )
                        btnTrack.setText( R.string.btn_gps_track_stop );
                }
            }
        });

        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance( this );
        IntentFilter filter = new IntentFilter();
        filter.addAction( BROADCAST_UPDATES );
        lbm.registerReceiver( new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long now = System.currentTimeMillis();
                Location location = intent.getParcelableExtra( LocationService.GPS_LOCATION );
                boolean isSingleFix = intent.getBooleanExtra( LocationService.GPS_FIRST_FIX, false );
                if ( location != null ) {
                    if ( !isSingleFix ) {
                        if ( firstLocation == null ) {
                            firstLocation = location;
                            map.animateCamera( CameraUpdateFactory.newLatLngZoom( new LatLng( location.getLatitude(), location.getLongitude() ), zoomLevel ));
                        }

                        if ( location.hasAccuracy() )
                            lblAccuracy.setText( String.format( "%.2fm", location.getAccuracy() ));
                        else
                            lblAccuracy.setText( "N/A" );

                        if ( start <= 0 )
                            start = System.currentTimeMillis();

                        double speed = Utils.round( Utils.toMph( location.getSpeed() ), 2 );
                        lblSpeed.setText( String.format( "%.2fmph", speed ));
                        maxSpeed = speed > maxSpeed ? speed : maxSpeed;

                        if ( lastLocation != null ) {
                            mileage += Utils.toMiles( lastLocation.distanceTo( location ) );
                            lblMileage.setText( String.format( "%.2fmi", mileage ) );

                            LatLng last = new LatLng( lastLocation.getLatitude(), lastLocation.getLongitude() );
                            LatLng cur = new LatLng( location.getLatitude(), location.getLongitude() );
                            map.addPolyline( new PolylineOptions()
                                    .add( last, cur )
                                    .width( lineWidth )
                                    .color( lineColor ) )
                                    .setGeodesic( true );
                        }

                        lastLocation = location;
                        if (( Math.abs( lastUpdate - now ) >= updateInterval )) {
                            if ( lastUpdateLocation == null || lastUpdateLocation.distanceTo( location ) >= updateDistance ) {
                                gpsPath.add( new JsonLocation( location.getLatitude(), location.getLongitude() ));
                                lastUpdate = now;
                                lastUpdateLocation = location;
                            }
                        }
                    } else {
                        map.animateCamera( CameraUpdateFactory.newLatLngZoom( new LatLng( location.getLatitude(), location.getLongitude() ), zoomLevel ));
                    }

                    map.moveCamera( CameraUpdateFactory.newLatLng( new LatLng( location.getLatitude(), location.getLongitude() )));
                }
            }
        }, filter );
    }
}
