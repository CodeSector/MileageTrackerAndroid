package net.codesector.Mileage.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate( Bundle saved ) {
        super.onCreate( saved );

        displayMenu = false;

        getActionBar().setDisplayHomeAsUpEnabled( true );
        getFragmentManager().beginTransaction().replace( android.R.id.content, new PreferencesFragment()).commit();
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        if ( item.getItemId() == android.R.id.home ) {
            setResult( 1, new Intent() );
            finish();

            return true;
        }

        return false;
    }
}
