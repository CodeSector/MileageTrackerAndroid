package net.codesector.Mileage.Activity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import net.codesector.Mileage.Api.ApiCallback;
import net.codesector.Mileage.Api.ApiClient;
import net.codesector.Mileage.Api.ApiError;
import net.codesector.Mileage.Config;
import net.codesector.Mileage.JsonLocation;
import net.codesector.Mileage.Loader;
import net.codesector.Mileage.Utils;
import net.codesector.Mileage.Model.Category;
import net.codesector.Mileage.Model.Trip;
import net.codesector.Mileage.R;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit.client.Response;

public class CreateTripActivity extends BaseActivity {

    //region Properties

    private ViewFlipper vfFlipper;
    private Button btnNext, btnBack, btnCreate;
    private RadioButton rbOdometer, rbManual;
    private EditText txtStart, txtEnd, txtMileage, txtTitle, txtNotes, txtMileageDisplay;
    private Spinner spCategory;

    private Double maxSpeed, averageSpeed;
    private Boolean isGPSTracked = false;

    private final static int MAX_NOTE_CHARS = 255;
    private final static int MAX_TITLE_CHARS = 128;
    private final static int PAGE_1 = 1;
    private final static int PAGE_2 = 2;

    //endregion

    //region Overrides

    @Override
    protected void onCreate( Bundle state ) {
        super.onCreate( state );

        getActionBar().setDisplayHomeAsUpEnabled( true );
        setContentView( R.layout.activity_create_trip);

        vfFlipper = (ViewFlipper) findViewById(R.id.vf_create_trip);

        btnNext = (Button) findViewById(R.id.btn_create_next);
        btnBack = (Button) findViewById(R.id.btn_create_back);
        btnCreate = (Button) findViewById(R.id.btn_create);

        rbOdometer = (RadioButton) findViewById( R.id.rb_odometer );
        rbManual = (RadioButton) findViewById( R.id.rb_manual );

        txtMileageDisplay = (EditText) findViewById( R.id.txt_mileage_display );

        txtStart = (EditText) findViewById( R.id.txt_odstart );
        txtEnd = (EditText) findViewById( R.id.txt_odend );
        txtMileage = (EditText) findViewById( R.id.txt_mileage );
        txtTitle = (EditText) findViewById( R.id.txt_title );
        txtNotes = (EditText) findViewById( R.id.txt_notes );

        spCategory = (Spinner) findViewById( R.id.sp_category );

        if ( state != null ) {
            vfFlipper.setDisplayedChild( state.getInt( Config.STATE_VIEW_FLIPPER_PAGE ));
        }

        double mileage = getIntent().getDoubleExtra( "mileage", 0 );
        final ArrayList<JsonLocation> gpsPath = getIntent().getParcelableArrayListExtra( "gps_data" );

        if ( mileage != 0 ) { // && !gpsPath.isEmpty() ) {
            isGPSTracked = true;

            maxSpeed = getIntent().getDoubleExtra( "max_speed", 0 );
            averageSpeed = getIntent().getDoubleExtra( "average_speed", 0 );

            btnBack.setVisibility( View.GONE );
            txtMileage.setText( String.format( "%.2f", mileage ));
            txtMileageDisplay.setText( txtMileage.getText() );

            String title = getIntent().getStringExtra( "title" );
            if ( title != null && !title.isEmpty() )
                txtTitle.setText( title );

            vfFlipper.setDisplayedChild( vfFlipper.indexOfChild( findViewById( R.id.vp_create_trip_page2 )));
        }

        View.OnClickListener rbListener = new View.OnClickListener() {
            public void onClick( View v ) {
                boolean checked = ((RadioButton) v).isChecked();

                if ( checked ) {
                    switch (v.getId()) {
                        case R.id.rb_odometer:
                            rbManual.setChecked( false );
                            ToggleInputs( false );
                            CalcMileage();
                            break;
                        case R.id.rb_manual:
                            rbOdometer.setChecked( false );
                            ToggleInputs( true );
                            break;
                    }
                }
            }
        };
        rbOdometer.setOnClickListener( rbListener );
        rbManual.setOnClickListener( rbListener );

        TextWatcher watcher = new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) { }
            @Override public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                CalcMileage();
            }
        };
        txtStart.addTextChangedListener( watcher );
        txtEnd.addTextChangedListener( watcher );

        txtMileage.addTextChangedListener( new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                txtMileageDisplay.setText( txtMileage.getText() );
                btnNext.setEnabled( ( Utils.parseDouble(txtMileage.getText().toString()) != 0.0 ));
            }
        });

        View.OnClickListener btnListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch ( view.getId() ) {
                    case R.id.btn_create_next:
                        vfFlipper.setInAnimation( AnimationUtils.loadAnimation( CreateTripActivity.this, R.anim.slide_in_right ));
                        vfFlipper.setOutAnimation( AnimationUtils.loadAnimation( CreateTripActivity.this, R.anim.slide_out_left ));

                        if ( Validate( PAGE_1 ) )
                            vfFlipper.showNext();

                        break;
                    case R.id.btn_create_back:
                        if ( !isGPSTracked ) {
                            vfFlipper.setInAnimation(AnimationUtils.loadAnimation(CreateTripActivity.this, R.anim.slide_in_left));
                            vfFlipper.setOutAnimation(AnimationUtils.loadAnimation(CreateTripActivity.this, R.anim.slide_out_right));
                            vfFlipper.showPrevious();
                        }
                        break;
                }
            }
        };
        btnNext.setOnClickListener( btnListener );
        btnBack.setOnClickListener( btnListener );

        final TextView lblNoteChars = (TextView) findViewById( R.id.lbl_notes_chars );
        final TextView lblTitleChars = (TextView) findViewById( R.id.lbl_title_chars );

        lblNoteChars.setText( "0/" + MAX_NOTE_CHARS );
        lblTitleChars.setText( "0/" + MAX_TITLE_CHARS );

        InputFilter[] filter = new InputFilter[1];
        filter[0] = new InputFilter.LengthFilter( MAX_NOTE_CHARS );
        txtNotes.setFilters( filter );
        txtNotes.addTextChangedListener( new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                lblNoteChars.setText( String.format( "%d/%d", txtNotes.getText().toString().length(), MAX_NOTE_CHARS ) );
            }
        });

        InputFilter[] filter2 = new InputFilter[1];
        filter2[0] = new InputFilter.LengthFilter( MAX_NOTE_CHARS );
        txtTitle.setFilters( filter2 );
        txtTitle.addTextChangedListener( new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                lblTitleChars.setText( String.format( "%d/%d", txtTitle.getText().toString().length(), MAX_NOTE_CHARS ) );
            }
        });

        ArrayList<Category> categories = new ArrayList<>();
        Iterator<Category> i = Category.findAll(Category.class);
        while( i.hasNext() ) {
            Category c = i.next();
            categories.add( c );
        }

        ArrayAdapter<Category> adapter = new ArrayAdapter<>( this, android.R.layout.simple_spinner_dropdown_item, categories );
        spCategory.setAdapter( adapter );

        btnCreate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View view ) {
                btnCreate.setEnabled( false );

                if ( Validate( PAGE_2 )) {
                    FragmentManager fm = getFragmentManager();
                    final Loader loader = new Loader();
                    loader.setMessage( "Creating..." );
                    loader.show( fm, "loader" );

                    Trip t = new Trip();
                    Double mileage = Utils.round( Utils.parseDouble( txtMileage.getText().toString() ), 2 );
                    Category cat = (Category) spCategory.getSelectedItem();
                    t.setCategory( cat );
                    t.setUser( user );
                    t.setMileage( mileage );

                    String title = txtTitle.getText().toString();
                    if ( !title.isEmpty() && !title.equals( "" ))
                        t.setTitle( title );

                    String notes = txtNotes.getText().toString();
                    if ( !notes.isEmpty() && !notes.equals( "" ))
                        t.setNotes( notes );

                    if ( maxSpeed != null && maxSpeed > 0f )
                        t.setMaxSpeed( maxSpeed );

                    if ( averageSpeed != null && averageSpeed > 0f )
                        t.setAverageSpeed( averageSpeed );

                    boolean sendGpsData = preferences.getBoolean( getResources().getString( R.string.pref_send_gps_path ), false );
                    if ( isGPSTracked && sendGpsData && !gpsPath.isEmpty() )
                        t.setGpsPath( gpsPath );

                    ApiClient.forge().service().createTrip( t, new ApiCallback<Trip>() {
                        @Override
                        public void success(Trip trip, Response response) {
                            loader.dismiss();
                            Toast.makeText( getApplicationContext(), "Trip Created Successfully!", Toast.LENGTH_LONG ).show();
                            finish();
                        }

                        @Override
                        public void failure(ApiError e) {
                            loader.dismiss();
                            new AlertDialog.Builder( CreateTripActivity.this ).setTitle( "API Error!" )
                                    .setMessage(TextUtils.join("\n", Utils.prependTo("\u2022 ", e.errors.get(""))))
                                    .setNeutralButton("OK", null).show();

                            btnCreate.setEnabled( true );
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState( Bundle state ) {
        state.putInt( Config.STATE_VIEW_FLIPPER_PAGE, vfFlipper.getDisplayedChild() );

        super.onSaveInstanceState( state );
    }

    //endregion

    //region Methods

    private boolean Validate( int id ) {
        boolean status = true;

        switch ( id ) {
            case PAGE_1:
                if ( txtMileage.getText().toString().isEmpty() || txtMileage.getText().toString() == null ) {
                    txtMileage.setError( "Mileage is required!" );
                    status = false;
                }

                break;
            case PAGE_2:
                //Todo: Possibly Add Validation for Page 2
                break;
        }

        return status;
    }

    private void CalcMileage() {
        double start = Utils.parseDouble( txtStart.getText().toString() );
        double end = Utils.parseDouble( txtEnd.getText().toString() );

        txtMileage.setText( String.valueOf( Math.abs( start - end)));
    }

    private void ToggleInputs( boolean b ) {
        txtMileage.setEnabled( b );
        txtStart.setEnabled( !b );
        txtEnd.setEnabled( !b );
    }

    //endregion
}
