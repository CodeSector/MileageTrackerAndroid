package net.codesector.Mileage.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import net.codesector.Mileage.Api.ApiClient;
import net.codesector.Mileage.Config;
import net.codesector.Mileage.Model.User;
import net.codesector.Mileage.R;

public class BaseActivity extends Activity {
    protected SharedPreferences preferences;

    protected static User user;

    protected boolean displayMenu = true;

    @Override
    protected void onCreate( Bundle state ) {
        super.onCreate( state );

        //Mint.initAndStartSession( this, Config.SPLUNK_MINT_API_KEY );
        preferences = PreferenceManager.getDefaultSharedPreferences( getApplicationContext() );

        // Set Default Preferences on First Run
        PreferenceManager.setDefaultValues( getApplicationContext(), R.xml.preferences, false );

        if ( ApiClient.forge() == null ) {
            Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            User u = g.fromJson( preferences.getString( Config.PREF_USER, null ), User.class );

            if ( u != null && u.getPassword() != null && !u.getPassword().isEmpty() && u.getUsername() != null && !u.getUsername().isEmpty()) {
                ApiClient.forge( u.getUsername(), u.getPassword() );
            } else {
                User.logout( this );
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int res = GooglePlayServicesUtil.isGooglePlayServicesAvailable( getApplicationContext() );

        if ( res != ConnectionResult.SUCCESS ) {
            if ( GooglePlayServicesUtil.isUserRecoverableError( res )) {
                Dialog err = GooglePlayServicesUtil.getErrorDialog(res, this, 1001, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        KillApplication();
                    }
                });

                if ( err != null )
                    err.show();
            } else {
                KillApplication();
            }
        }
    }

    @Override
    public void onConfigurationChanged( Configuration conf ) {
        super.onConfigurationChanged( conf );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        if ( displayMenu ) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate( R.menu.main, menu );
            menu.findItem(R.id.actionbar_logout).setIcon(new IconDrawable(this, Iconify.IconValue.fa_power_off).colorRes(R.color.white).actionBarSize());
            menu.findItem(R.id.actionbar_settings).setIcon(new IconDrawable(this, Iconify.IconValue.fa_cogs).colorRes(R.color.white).actionBarSize());
        }

        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch (item.getItemId()) {
            case R.id.actionbar_logout:
                User.logout( this );

                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.actionbar_settings:
                OpenSettings();

                return true;
            default:
                return super.onOptionsItemSelected( item );
        }
    }

    @Override
    protected void onActivityResult( int request, int result, Intent i ) {
        //TODO: Dunno figure it out
    }

    protected void OpenSettings() {
        Intent i = new Intent( this, SettingsActivity.class );
        startActivityForResult( i, 0 );
    }

    protected void KillApplication() {
        Toast.makeText( this, "This application requires Google Play Services", Toast.LENGTH_SHORT ).show();
        finish();
        System.exit( 0 );
    }
}
