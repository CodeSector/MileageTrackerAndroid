package net.codesector.Mileage.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.codesector.Mileage.Api.ApiClient;
import net.codesector.Mileage.Api.ApiService;
import net.codesector.Mileage.Config;
import net.codesector.Mileage.Model.Category;
import net.codesector.Mileage.Model.User;
import net.codesector.Mileage.R;

import java.util.List;

public class SplashScreen extends BaseActivity {
    @Override
    protected void onCreate( Bundle state ) {
        super.onCreate(state);
        setContentView( R.layout.activity_splash );

        new PrefetchData().execute();
    }

    public void showToast( final String toast ) {
        runOnUiThread( new Runnable() {
            @Override
            public void run() {
                Toast.makeText( getApplicationContext(), toast, Toast.LENGTH_LONG ).show();
            }
        } );
    }

    private class PrefetchData extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() { super.onPreExecute(); }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            User u = g.fromJson( preferences.getString( Config.PREF_USER, null ), User.class );

            try {
                // Verify that we have login info
                if (u != null && u.getPassword() != null && !u.getPassword().isEmpty() && u.getUsername() != null && !u.getUsername().isEmpty()) {
                    // Attempt a login to make sure the credentials are uptodate
                   // ApiService service = ApiClient.forge(user.getUsername(), user.getPassword(), true).service();
                    ApiService service = ApiClient.forge( u.getUsername(), u.getPassword(), true ).service();
                    u = service.login();
                    if ( u != null ) {
                        // Now that we have a valid user, query the server for various required information
                        // and store it in the BaseActivity instance for the duration of the application
                        // this way, all activities will have access to the categories and it will be updated when
                        // the application starts up.
                        user = u;

                        // Empty Category cache and re-populate it from the server
                        Category.truncate();
                        List<Category> cats = service.getCategories();
                        for( Category cat : cats )
                            cat.save();

                        return true;
                    } else
                        return false;
                }
            } catch (Exception ex) {
                // Todo: Handle connection errors and present them to the user
                showToast( "ApiError - " + ex.getLocalizedMessage() );
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute( result );

            Intent i = ( result ) ? new Intent( SplashScreen.this, DashboardActivity.class ) : new Intent( SplashScreen.this, LoginActivity.class );
            startActivity( i );

            finish();
        }
    }
}
