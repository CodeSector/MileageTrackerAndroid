package net.codesector.Mileage.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import net.codesector.Mileage.Model.User;
import net.codesector.Mileage.R;

public class PreferencesFragment extends PreferenceFragment {

    private Preference resetButton;

    @Override
    public void onCreate( Bundle saved ) {
        super.onCreate( saved );

        addPreferencesFromResource( R.xml.preferences );

        final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences( getActivity() ).edit();
        Preference logout = (Preference) getPreferenceManager().findPreference( "logout" );
        if ( logout != null )
            logout.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    User.logout( getActivity() );

                    return true;
                }
            });

        resetButton = getPreferenceScreen().findPreference( "reset" );
        resetButton.setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                getFragmentManager().beginTransaction().replace( android.R.id.content, new PreferencesFragment()).commit();

                return false;
            }
        });

    }
}
