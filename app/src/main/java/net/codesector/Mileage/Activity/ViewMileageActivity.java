package net.codesector.Mileage.Activity;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.codesector.Mileage.Model.Trip;
import net.codesector.Mileage.R;
import net.codesector.Mileage.Service.TripLoader;
import net.codesector.Mileage.TripListViewAdapter;

import java.util.ArrayList;

public class ViewMileageActivity extends BaseActivity  {
    private ListView lvTrips;

    @Override
    protected void onCreate( Bundle saved ) {
        super.onCreate( saved );

        getActionBar().setDisplayHomeAsUpEnabled( true );
        setContentView( R.layout.activity_view_mileage );

        final TripListViewAdapter adapter = new TripListViewAdapter( this, new ArrayList<Trip>() );
        lvTrips = (ListView) findViewById( R.id.lv_trips );
        lvTrips.setAdapter( adapter );
        lvTrips.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
                Trip t = (Trip) lvTrips.getItemAtPosition( position );
                System.out.println( t.toString() );
            }
        } );

        getLoaderManager().initLoader( 0, saved, new LoaderManager.LoaderCallbacks<ArrayList<Trip>>() {
            @Override
            public Loader<ArrayList<Trip>> onCreateLoader( int id, Bundle args ) {
                return new TripLoader( ViewMileageActivity.this );
            }

            @Override
            public void onLoadFinished( Loader<ArrayList<Trip>> loader, ArrayList<Trip> data ) {
                adapter.setTrips( data );
            }

            @Override
            public void onLoaderReset( Loader<ArrayList<Trip>> loader ) {
                adapter.setTrips( new ArrayList<Trip>() );
            }
        } ).forceLoad();
    }
}
