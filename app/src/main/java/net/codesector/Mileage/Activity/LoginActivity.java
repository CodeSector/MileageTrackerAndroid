package net.codesector.Mileage.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.codesector.Mileage.Api.ApiClient;
import net.codesector.Mileage.Config;
import net.codesector.Mileage.Model.User;
import net.codesector.Mileage.R;

public class LoginActivity extends Activity {
    protected SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_login_activity);

        //Mint.initAndStartSession( this, Config.SPLUNK_MINT_API_KEY );

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        final EditText txtUsername = (EditText) findViewById( R.id.txt_username );
        final EditText txtPassword = (EditText) findViewById( R.id.txt_password );

        //region UI Events

        // Login Handler
        final Button btnLogin = (Button) findViewById( R.id.btn_login );
        btnLogin.setEnabled( false );
        btnLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick (View view ) {
                // TODO: Log user in, get API Key encrypt it and store it in preferences
                String username = txtUsername.getText().toString(), password = txtPassword.getText().toString();
                User.login( password, ApiClient.forge( username, password, true).api(), LoginActivity.this, preferences.edit(), new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent( LoginActivity.this, SplashScreen.class );
                        startActivity( i );

                        finish();
                    }
                });
            }
        });

        // Text changed button enabler
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {}

            @Override
            public void afterTextChanged(Editable editable) {
                boolean ready = txtUsername.getText().length() > 3 && txtPassword.getText().length() >= 8;
                btnLogin.setEnabled(ready);
            }
        };

        txtUsername.addTextChangedListener( watcher );
        txtPassword.addTextChangedListener( watcher );

        //endregion
    }

}
