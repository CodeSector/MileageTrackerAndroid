package net.codesector.Mileage.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import net.codesector.Mileage.Model.User;
import net.codesector.Mileage.R;

public class DashboardActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate( Bundle state ) {
        super.onCreate( state );

        displayMenu = false;

        setContentView( R.layout.activity_main );

        Button btnGps = (Button) findViewById( R.id.btn_dashboard_gps );
        Button btnManual = (Button) findViewById( R.id.btn_dashboard_manual );
        Button btnView = (Button) findViewById( R.id.btn_dashboard_view );
        Button btnSetting = (Button) findViewById( R.id.btn_dashboard_settings );
        Button btnLogout = (Button) findViewById( R.id.btn_dashboard_logout );

        btnGps.setOnClickListener( this );
        btnManual.setOnClickListener( this );
        btnView.setOnClickListener( this );
        btnSetting.setOnClickListener( this );
        btnLogout.setOnClickListener( this );
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch (item.getItemId()) {
            case R.id.actionbar_logout:
                User.logout( this );

                return true;
            default:
                return super.onOptionsItemSelected( item );
        }
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch( view.getId() ) {
            case R.id.btn_dashboard_gps:
                i = new Intent( DashboardActivity.this, GPSTrackActivity.class );
                break;
            case R.id.btn_dashboard_manual:
                i = new Intent( DashboardActivity.this, CreateTripActivity.class );
                break;
            case R.id.btn_dashboard_settings:
                OpenSettings();
                break;
            case R.id.btn_dashboard_logout:
                User.logout( this );
                break;
            case R.id.btn_dashboard_view:
                i = new Intent( DashboardActivity.this, ViewMileageActivity.class );
                break;
        }

        if ( i != null )
            startActivity( i );
    }
}
