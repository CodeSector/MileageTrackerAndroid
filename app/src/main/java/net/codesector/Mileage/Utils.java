package net.codesector.Mileage;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Utils {
    public static String[] prependTo( String text, String... items) {

        for ( int i = 0; i < items.length; i++ )
            items[i] = text + items[i];
        return items;
    }

    public static double parseDouble( String s ) {
        return ( s == null || s.isEmpty() ) ? 0.0 : Double.parseDouble( s );
    }

    public static double round( double value, int places ) {
        if (places < 0) throw new IllegalArgumentException( "Places must be a positive number." );

        BigDecimal dec = new BigDecimal( value );
        return dec.setScale( places, RoundingMode.HALF_UP ).doubleValue();
    }

    public static double toMiles( double d ) {
        return d * 0.000621371192;
    }

    public static double toMeters( double d ) {
        return d * 1609.344;
    }

    public static double toMph( double d ) {
        if ( d < 1f )
            return 0f;
        return (( d * 3600 ) *  0.000621371192 );
    }

    public static double toHours( double t ) { return t / ((double)( 1000 * 60 * 60 ));}
    public static double timeSpan( double t1, double t2 ) {
        return Math.abs( t1 - t2 );
    }

    public static void HandleError() {

    }

    public static String locationToString( Location loc, Context context ) {
        return Utils.locationToString( loc.getLatitude(), loc.getLongitude(), context );
    }

    public static String locationToString( double lat, double lng, Context context ) {
        String value = "";
        try {
            Geocoder geocoder = new Geocoder( context );
            List<Address> list = geocoder.getFromLocation( lat, lng, 1 );
            Address address = list.get( 0 );
            value = address.getAddressLine( 0 );
            if ( value == null || value.isEmpty() )
                value = address.getLocality();
        } catch ( Exception ex ) { }

        return value;
    }
}
