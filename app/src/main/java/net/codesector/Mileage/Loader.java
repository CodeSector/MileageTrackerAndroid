package net.codesector.Mileage;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;

public class Loader extends DialogFragment {
    private String message = "Working...";
    public Loader() { }

    public void setMessage( String message ) {
        this.message = message;
    }

    @Override
    public Dialog onCreateDialog( final Bundle state ) {
        ProgressDialog dialog = new ProgressDialog( getActivity() );
        this.setStyle( STYLE_NO_TITLE, getTheme() );
        dialog.setMessage( this.message );

        dialog.setCancelable( false );
        return dialog;
    }

}
