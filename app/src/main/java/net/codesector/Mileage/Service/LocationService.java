package net.codesector.Mileage.Service;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;

public class LocationService extends Service implements LocationListener, GpsStatus.Listener {

    //region Properties

    public static final int GPS_FIXED = 1;
    public static final int GPS_LOST = 2;
    public static final String GPS_STATUS = "status";
    public static final String GPS_LOCATION = "location";
    public static final String GPS_FIRST_FIX = "first_fix";

    private final Context _context;
    private Intent _receiver;
   // private long _gpsInterval = 3000, _minDistance = 10; // 3 seconds, 10 meters
    private long _elapsed;
    boolean isFirstFix = true, isGPSEnabled = false, isNetworkEnabled = false, canTrack = false, isGPSFix = false;

    protected LocationManager _lm;

    private Location _location;

    //endregion

    //region Getters & Setters, Constructor(s)

    public boolean isGPSEnabled() {
        return isGPSEnabled;
    }

    public boolean isNetworkEnabled() {
        return isNetworkEnabled;
    }

    public boolean canTrack() {
        return canTrack;
    }

    public Location getLocation() {
        return _location;
    }

    public LocationService( Context context, String receiver ) {
        this._context = context;
        this._receiver = new Intent( receiver );

        init();
    }

    //endregion

    protected void init() {
        try {
            _lm = ( LocationManager ) _context.getSystemService( LOCATION_SERVICE );
            isGPSEnabled = _lm.isProviderEnabled( LocationManager.GPS_PROVIDER );
            isNetworkEnabled = _lm.isProviderEnabled( LocationManager.NETWORK_PROVIDER );

            if ( isGPSEnabled || isNetworkEnabled ) {

                Location gpsLoc = null, netLoc = null;
                if ( _lm != null ) {
                    if ( isGPSEnabled ) {
                        gpsLoc = _lm.getLastKnownLocation( LocationManager.GPS_PROVIDER );

                        isFirstFix = false;
                       /* _lm.requestSingleUpdate( LocationManager.GPS_PROVIDER, this, null );*/
                    }
                    if ( isNetworkEnabled )
                        netLoc = _lm.getLastKnownLocation( LocationManager.NETWORK_PROVIDER );
                }

                _location = isBetterLocation( gpsLoc, netLoc ) ? gpsLoc : netLoc;
            }

            if ( !isGPSEnabled ) {
                canTrack = false;
                showSettingsAlert();
            } else
                canTrack = true;
        } catch ( Exception e ) {
            // TODO: Error handling
        }
    }

  /*  public void setInterval( int seconds ) {
        this._gpsInterval = seconds;
    }

    public void setDistance( int meters ) {
        this._minDistance = meters;
    }*/

    public boolean start() {
        init();
        if ( _lm != null ) {
            if ( isGPSEnabled ) {
                _lm.requestLocationUpdates( LocationManager.GPS_PROVIDER, 250, 0, this );
                /*if ( isNetworkEnabled )
                    _lm.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, this );*/
            }

            return isGPSEnabled;
            // NotificationManager nm = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
            // _lm.requestLocationUpdates( LocationManager.GPS_PROVIDER, _gpsInterval, _minDistance, this );

            /*Notification note = new Notification.Builder( _context )
                    .setContentTitle( "GPS Tracking Active" )
                    .setContentText( "Click to bring Mileage into focus" )
                    .setSmallIcon( R.drawable.ic_launcher )
                    .build();

            nm.notify( 1, note );*/
        } else {
            // This line should never be run...but just in case
            showSettingsAlert();
            return false;
        }
    }

    public void stop() {
        if ( _lm != null ) {
            _lm.removeUpdates( LocationService.this );
            /*NotificationManager nm = (NotificationManager) getSystemService( NOTIFICATION_SERVICE );
            nm.cancel( 1 );*/
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder dlg = new AlertDialog.Builder( _context );
        dlg.setTitle( "GPS is disabled" );
        dlg.setMessage( "GPS is required for precise tracking. Please enable it to continue." );
        dlg.setPositiveButton( "Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialogInterface, int j ) {
                Intent i = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                _context.startActivity( i );
            }
        } );

        dlg.setNegativeButton( "Ignore", new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int i ) {
                dialog.cancel();
            }
        } );
        dlg.show();
    }

    public static boolean isBetterLocation( Location location, Location currentBestLocation ) {
        if ( currentBestLocation == null ) {
            // A new location is always better than no location
            return true;
        }

        if ( location == null ) {
            // Old location is always better than no location
            return false;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > ( 60 * 1000L );
        boolean isSignificantlyOlder = timeDelta < -( 60 * 1000L );
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location,
        // use the new location
        // because the user has likely moved
        if ( isSignificantlyNewer )
            return true;
            // If the new location is more than two minutes older, it must
            // be worse
        else if ( isSignificantlyOlder )
            return false;

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = ( int ) ( location.getAccuracy() - currentBestLocation.getAccuracy() );
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider( location.getProvider(), currentBestLocation.getProvider() );

        // Determine location quality using a combination of timeliness and
        // accuracy
        if ( isMoreAccurate ) {
            return true;
        } else if ( isNewer && !isLessAccurate ) {
            return true;
        } else if ( isNewer && !isSignificantlyLessAccurate && isFromSameProvider ) {
            return true;
        }
        return false;
    }

    public static boolean isSameProvider( String provider1, String provider2 ) {
        if ( provider1 == null )
            return provider2 == null;
        return provider1.equals( provider2 );
    }

    //region Overrides

    @Override
    public IBinder onBind( Intent intent ) {
        return null;
    }

    @Override
    public void onStatusChanged( String s, int code, Bundle bundle ) {
    }

    @Override
    public void onProviderEnabled( String s ) {
    }

    @Override
    public void onProviderDisabled( String s ) {
    }

    @Override
    public void onGpsStatusChanged( int e ) {
        switch ( e ) {
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                if ( _location != null )
                    isGPSFix = ( SystemClock.elapsedRealtime() - _elapsed ) < 3000;

                if ( !isGPSFix ) {
                    _receiver.putExtra( GPS_STATUS, GPS_LOST );
                }
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                isGPSFix = true;
                break;
        }
    }

    @Override
    public void onLocationChanged( Location location ) {
        if ( location == null ) return;

        // If the new location is considered better than what we have,
        // use the new one otherwise just ignore the update
        if ( isBetterLocation( location, _location ) ) {
            _elapsed = SystemClock.elapsedRealtime();
            _location = location;
            if ( _receiver != null ) {
                if ( location.hasAccuracy() && location.getAccuracy() < 100f ) {
                    _receiver.putExtra( GPS_LOCATION, location );
                    _receiver.putExtra( GPS_STATUS, GPS_FIXED );
                    _receiver.putExtra( GPS_FIRST_FIX, isFirstFix );
                    LocalBroadcastManager.getInstance( this ).sendBroadcast( _receiver );

                    if ( isFirstFix )
                        isFirstFix = false;
                }
            }
        }
    }

    //endregion
}
