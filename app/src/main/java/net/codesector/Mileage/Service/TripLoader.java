package net.codesector.Mileage.Service;

import android.content.AsyncTaskLoader;
import android.content.Context;

import net.codesector.Mileage.Api.ApiClient;
import net.codesector.Mileage.Model.Trip;

import java.util.ArrayList;

public class TripLoader extends AsyncTaskLoader<ArrayList<Trip>> {

    public TripLoader( Context context ) {
        super( context );
    }

    @Override
    public ArrayList<Trip> loadInBackground() {
        return ApiClient.forge().service().getTrips();
    }
}
