package net.codesector.Mileage;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;

public class JsonLocation implements Parcelable {

    //region Properties

    @Expose private double lat;
    @Expose private double lng;

    //endregion

    //region Constructors

    public JsonLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public JsonLocation( Parcel in ) {
        double[] data = new double[2];
        in.readDoubleArray( data );
        this.lat = data[0];
        this.lng = data[1];
    }

    //endregion

    //region Getters & Setters

    public void setLat( double lat ) { this.lat = lat; }
    public void setLng( double lng ) { this.lng = lng; }

    public double getLat() { return lat; }
    public double getLng() { return lng; }

    public LatLng toLatLng() { return new LatLng( lat, lng ); }

    //endregion

    //region Overrides

    @Override public String toString() {
        return String.format( "Latitude: %f, Longitude: %f", lat, lng );
    }

    //endregion

    //region Parcelable

    @Override public int describeContents() { return 0; }
    @Override public void writeToParcel( Parcel out, int flags ) {
        out.writeDoubleArray( new double[] { lat, lng });
    }

    public static final Creator CREATOR = new Creator() {
        public JsonLocation createFromParcel( Parcel in ) {
            return new JsonLocation( in );
        }

        public JsonLocation[] newArray( int size ) {
            return new JsonLocation[size];
        }
    };

    //endregion
}
