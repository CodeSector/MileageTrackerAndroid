package net.codesector.Mileage;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.codesector.Mileage.Model.Category;
import net.codesector.Mileage.Model.Trip;

import java.util.ArrayList;

public class TripListViewAdapter extends BaseAdapter {

    protected ArrayList<Trip> list;
    protected Context context;

    public TripListViewAdapter( Context context, ArrayList<Trip> list ) {
        super();

        this.context = context;
        this.list = list;
    }

    public void setTrips( ArrayList<Trip> trips ) {
        if ( list != null ) {
            list.clear();
        } else {
            list = new ArrayList<Trip>();
        }

        if ( trips != null)
            list.addAll( trips );

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem( int position ) {
        return list.get( position );
    }

    @Override
    public long getItemId( int position ) {
        return 0;
    }

    @Override
    public View getView( int position, View view, ViewGroup parent ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View v = ( view != null ) ? view : inflater.inflate( R.layout.list_item_trip, parent, false );

        TextView lblTitle = (TextView) v.findViewById( R.id.lbl_title );
        TextView lblDate = (TextView) v.findViewById( R.id.lbl_date );
        TextView lblCategory = (TextView) v.findViewById( R.id.lbl_category );
        TextView lblDetails = (TextView) v.findViewById( R.id.lbl_details );
        TextView lblMileage = (TextView) v.findViewById( R.id.lbl_mileage );

        Trip t = list.get( position );

        Category category = t.getCategory();
        String cat = category.getTitle();
        String notes = t.getNotes();
        String title = t.getTitle();

        lblTitle.setText( ( title != null && !title.isEmpty() ) ? title : "(none)" );
        lblCategory.setText( ( cat != null && !cat.isEmpty() ) ? cat : "(none)" );
        lblDetails.setText(( notes == null || notes.isEmpty() ? "(none)" : notes.replaceAll( "\n", " " ) ));
        lblMileage.setText( String.format( "%.2fm", t.getMileage() ));

        lblDate.setText( DateUtils.getRelativeTimeSpanString( t.getCreated(), System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS  ) );

        return v;
    }
}
