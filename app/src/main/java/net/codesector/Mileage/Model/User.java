package net.codesector.Mileage.Model;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import net.codesector.Mileage.Api.ApiCallback;
import net.codesector.Mileage.Api.ApiError;
import net.codesector.Mileage.Api.ApiService;
import net.codesector.Mileage.Config;
import net.codesector.Mileage.Utils;
import net.codesector.Mileage.Activity.LoginActivity;

import retrofit.RestAdapter;
import retrofit.client.Response;

public class User implements Parcelable {
    @Expose private Integer id;
    @Expose private String username;
    @Expose private String fullname;
    @Expose private String email;
    @Expose private Group group;

    @Expose private String password;

    //region Getters & Setters

    public Integer getId() { return id; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String getEmail() { return email; }
    public Group getGroup() { return group; }

    //endregion

    public User() { }

    public static void login( final String password, RestAdapter api, final Activity activity, final SharedPreferences.Editor editor, final Runnable success ) {
        if ( password.isEmpty() )
            return;

        ApiService loginService = api.create( ApiService.class );
        loginService.login( new ApiCallback<User>() {
            @Override
            public void success(User user, Response response) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                user.password = password;
                editor.putString( Config.PREF_USER, gson.toJson( user ));
                editor.apply();

                if ( success != null )
                    success.run();
            }

            @Override
            public void failure(ApiError e) {
                new AlertDialog.Builder( activity ).setTitle("Login Error")
                        .setMessage(TextUtils.join("\n", Utils.prependTo("\u2022 ", e.errors.get(""))))
                        .setNeutralButton("OK", null).show();
            }
        });

    }

    public static void logout( Activity activity ) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences( activity.getApplicationContext() );
        SharedPreferences.Editor editor = preferences.edit();

        editor.remove( Config.PREF_USER );
        editor.apply();

        Intent i = new Intent( activity, LoginActivity.class );
        i.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK );
        activity.startActivity( i );

        activity.finish();
    }

    //region Parcelable

    public User( Parcel in ) {
        String[] data = new String[3];
        in.readStringArray( data );

        this.username = data[0];
        this.fullname = data[1];
        this.email = data[2];
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel( Parcel out, int i ) {
        out.writeStringArray( new String[] { this.username, this.fullname, this.email } );
        out.writeParcelable( group, i );
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public User createFromParcel(Parcel in) {
            return new User( in );
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    //endregion
}
