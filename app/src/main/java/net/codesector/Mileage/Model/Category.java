package net.codesector.Mileage.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Category extends SugarRecord {
    @SerializedName( "id" )
    @Expose private Integer webID;
    @Expose private String title;

    //region Getters & Setters

    public Integer getWebID() { return webID; }
    public String getTitle() { return title; }

    public void setWebID( Integer id ) { this.webID = id; }
    public void setTitle( String title ) { this.title = title; }

    //endregion

    @Override
    public String toString() { return title; }

    public static void truncate() {
        try {
            Category.deleteAll( Category.class );
           // Category.executeQuery( "DELETE FROM SQLITE_SEQUENCE WHERE name='" + Category.getTableName( Category.class ) +"'" );
        } catch (Exception e) {

        }
    }
}
