package net.codesector.Mileage.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

public class Group implements Parcelable {
    @Expose public Integer id;
    @Expose public String name;

    public Group( Parcel in ) {
        this.id = in.readInt();
        this.name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt( id );
        out.writeString( name );
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public Group createFromParcel(Parcel in) {
            return new Group( in );
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };
}
