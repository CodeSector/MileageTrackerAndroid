package net.codesector.Mileage.Model;

import com.google.gson.annotations.Expose;

import net.codesector.Mileage.JsonLocation;

import java.util.ArrayList;

public class Trip {
    @Expose private Integer id;
    @Expose private User user;
    @Expose private Category category;
    @Expose private String title;
    @Expose private Double mileage;
    @Expose private Double maxSpeed;
    @Expose private Double averageSpeed;
    @Expose private ArrayList<JsonLocation> gpsPath;
    @Expose private String notes;
    @Expose private long created;
    @Expose private long updated;

    //region Getters & Setters

    public Integer getId() { return id; }
    public User getUser() { return user; }
    public Category getCategory() { return category; }
    public String getTitle() { return title; }
    public Double getMileage() { return mileage; }
    public Double getMaxSpeed() { return maxSpeed; }
    public Double getAverageSpeed() { return averageSpeed; }
    public ArrayList<JsonLocation> getGpsPath() { return gpsPath; }
    public String getNotes() { return notes; }
    public long getCreated() { return created; }
    public long getUpdated() { return updated; }

    public void setId(Integer id) { this.id = id; }
    public void setUser(User user) { this.user = user; }
    public void setCategory(Category category) { this.category = category; }
    public void setTitle( String title ) { this.title = title; }
    public void setMileage(Double mileage) { this.mileage = mileage; }
    public void setMaxSpeed( Double maxSpeed ) { this.maxSpeed = maxSpeed; }
    public void setAverageSpeed( Double averageSpeed ) { this.averageSpeed = averageSpeed; }
    public void setGpsPath(ArrayList<JsonLocation> gpsPath) { this.gpsPath = gpsPath; }
    public void setNotes(String notes) { this.notes = notes; }
    public void setCreated(long created) { this.created = created; }
    public void setUpdated(long updated) { this.updated = updated; }

    //endregion

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", user=" + user +
                ", category=" + category +
                ", title='" + title + '\'' +
                ", mileage=" + mileage +
                ", maxSpeed=" + maxSpeed +
                ", averageSpeed=" + averageSpeed +
                ", gpsPath=" + gpsPath +
                ", notes='" + notes + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
}
