package net.codesector.Mileage;

public class Config {
    public static final String API_VERSION = "v1";
    public static final String API_BASE = "https://itms.springscs.org";

    public static final String PREF_USER = "user";

    public static final String SPLUNK_MINT_API_KEY = "2f6df49e";

    public static final String STATE_VIEW_FLIPPER_PAGE = "VIEW_FLIPPER_PAGE";
    public static final String STATE_API_CLIENT = "API_CLIENT";
}
