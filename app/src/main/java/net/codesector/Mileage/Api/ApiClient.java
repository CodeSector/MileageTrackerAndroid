package net.codesector.Mileage.Api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.codesector.Mileage.Config;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class ApiClient {
    private static ApiClient _instance = null;

    private RestAdapter _adapter;

    private ApiClient( String username, String password ) {
        ApiRequestIntercepter interceptor = new ApiRequestIntercepter();
        interceptor.setUser( username, password );

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        _adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Config.API_BASE)
                .setRequestInterceptor(interceptor)
                .setClient(new OkClient())
                .setConverter(new GsonConverter( gson ))
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                       /* switch ( cause.getResponse().getStatus() ) {
                            case 400:
                                cause.getBody();
                            case 401:

                        }*/
                        //Log.d("ApiError", cause.getMessage() + " " + cause.getResponse().getReason());
                        return cause;
                    }
                })
                .build();
    }

    public static ApiClient forge() { return _instance; }
    public static ApiClient forge( String username, String password ) { return forge( username, password, false ); }
    public static ApiClient forge( String username, String password, boolean force) {
        if ( _instance == null || force )
            _instance = new ApiClient( username, password );
        return _instance;
    }

    public RestAdapter api() { return _adapter; }
    public ApiService service() {
        return _adapter.create( ApiService.class );
    }
}
