package net.codesector.Mileage.Api;

import com.google.gson.annotations.Expose;

import java.util.Map;

public class ApiError {
    @Expose public String message;
    @Expose public Map<String, String[]> errors;

    public ApiError( String message ) {
        String[] s = { message };
        errors.put( "", s );
    }
}
