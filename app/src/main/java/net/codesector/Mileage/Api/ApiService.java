package net.codesector.Mileage.Api;

import net.codesector.Mileage.Model.Category;
import net.codesector.Mileage.Model.Trip;
import net.codesector.Mileage.Model.User;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface ApiService {

    //region GET

    @GET( "/api/{version}/trip" )
    ArrayList<Trip> getTrips();

    @GET( "/api/{version}/trip/{id}" )
    void getTrip( @Path( "id" ) Integer id, Callback<Trip> cb );

    @GET( "/api/{version}/category" )
    List<Category> getCategories();

    //endregion

    //region POST

    @POST( "/api/{version}/user/login" )
    void login( Callback<User> cb );

    @POST( "/api/{version}/user/login" )
    User login();

    @POST( "/api/{version}/trip" )
    void createTrip( @Body Trip t, ApiCallback<Trip> cb );

    //endregion POST
}
