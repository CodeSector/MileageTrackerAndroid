package net.codesector.Mileage.Api;

import android.util.Base64;

import net.codesector.Mileage.Config;

import retrofit.RequestInterceptor;

public class ApiRequestIntercepter implements RequestInterceptor {
    protected String username;
    protected String password;

    @Override
    public void intercept( RequestFacade request ) {
        final String auth = this.username + ":" + this.password;
        request.addHeader( "Authorization", "Basic " + Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP ));
        request.addPathParam( "version", Config.API_VERSION );
    }

    public void setUser( String username, String password ) {
        this.username = username;
        this.password = password;
    }
}
