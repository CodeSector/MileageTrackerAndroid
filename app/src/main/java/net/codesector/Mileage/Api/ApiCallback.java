package net.codesector.Mileage.Api;

import retrofit.Callback;
import retrofit.RetrofitError;

public abstract class ApiCallback<T> implements Callback<T> {
    public abstract void failure( ApiError error );

    @Override
    public void failure( RetrofitError error ) {
        if ( error.getBody() != null ) {
            ApiError err = (ApiError) error.getBodyAs(ApiError.class);

            if ( err != null ) {
                failure( err );
            } else {
                failure( new ApiError( error.getMessage() ));
            }
        }
    }
}
